exports.get = {
  rooms: [
    {
      name: 'Одноместный эконом',
      parameters: {
        rooms: 1,
        square: 10,
        residents: '1',
        price_start: 700
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi'
        ],
        hygiene: [
          'туалет',
          'умывальник',
          'полотенце',
          'душевая кабина на этаже'
        ],
        other: [
          'односпальная кровать',
          'встроенный стенной шкаф'
        ]
      },
      images: [
        '/assets/img/rooms/one-economy/1.jpg',
        '/assets/img/rooms/one-economy/2.jpg',
        '/assets/img/rooms/one-economy/3.jpg',
        '/assets/img/rooms/one-economy/4.jpg'
      ]
    },
    {
      name: 'Одноместный стандарт',
      parameters: {
        rooms: 1,
        square: 14,
        residents: '1',
        price_start: 1000
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi'
        ],
        hygiene: [
          'собственная ванная комната с душем',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки'
        ],
        other: [
          'односпальная кровать',
          'встроенный стенной шкаф',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/one-standard/1.jpg',
        '/assets/img/rooms/one-standard/2.jpg'
      ]
    },
    {
      name: 'Одноместный стандарт (полутораспальная/двуспальная кровать)',
      parameters: {
        rooms: 1,
        square: 16,
        residents: '2',
        price_start: 1200
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi',
          'холодильник'
        ],
        hygiene: [
          'собственная ванная комната с душем',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'полутораспальная/двуспальная кровать',
          'тапочки'
        ],
        other: [
          'встроенный стенной шкаф',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/one-standard-bed/1.jpg',
        '/assets/img/rooms/one-standard-bed/2.jpg',
        '/assets/img/rooms/one-standard-bed/3.jpg',
        '/assets/img/rooms/one-standard-bed/4.jpg',
        '/assets/img/rooms/one-standard-bed/5.jpg'
      ]
    },
    {
      name: 'Двухместный стандарт',
      parameters: {
        rooms: 1,
        square: 16,
        residents: '2',
        price_start: 1400
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi'
        ],
        hygiene: [
          'собственная ванная комната с душем',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки'
        ],
        other: [
          'две односпальные кровати',
          'встроенный стенной шкаф',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/two-standard/1.jpg',
        '/assets/img/rooms/two-standard/2.jpg',
        '/assets/img/rooms/two-standard/3.jpg',
        '/assets/img/rooms/two-standard/4.jpg'
      ]
    },
    {
      name: 'Одноместный полулюкс',
      parameters: {
        rooms: 1,
        square: 25,
        residents: '2',
        price_start: 1500
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi',
          'холодильник',
          'фен (по запросу)'
        ],
        hygiene: [
          'собственная ванная комната с душевой кабиной',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки'
        ],
        other: [
          'двуспальная кровать',
          'шкаф',
          'журнальный столик',
          'кресло',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/one-halfluxe/1.jpg',
        '/assets/img/rooms/one-halfluxe/2.jpg',
        '/assets/img/rooms/one-halfluxe/3.jpg',
        '/assets/img/rooms/one-halfluxe/4.jpg',
        '/assets/img/rooms/one-halfluxe/5.jpg',
        '/assets/img/rooms/one-halfluxe/6.jpg'
      ]
    },
    {
      name: 'Двухместный полулюкс',
      parameters: {
        rooms: 1,
        square: 30,
        residents: '2-3',
        price_start: 2000
      },
      equipment: {
        technology: [
          'холодильник',
          'TV',
          'Wi-Fi',
          'кондиционер /2 этаж/',
          'фен (по запросу)',
        ],
        hygiene: [
          'собственная ванная комната с душевой кабиной/ванной',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки',
        ],
        other: [
          'две односпальные кровати',
          'шкаф',
          'журнальный столик',
          'кресло',
          'диван',
          'стол',
        ]
      },
      images: [
        '/assets/img/rooms/two-halfluxe/1.jpg',
        '/assets/img/rooms/two-halfluxe/2.jpg',
        '/assets/img/rooms/two-halfluxe/3.jpg',
        '/assets/img/rooms/two-halfluxe/4.jpg',
        '/assets/img/rooms/two-halfluxe/5.jpg',
        '/assets/img/rooms/two-halfluxe/6.jpg',
        '/assets/img/rooms/two-halfluxe/7.jpg',
        '/assets/img/rooms/two-halfluxe/8.jpg',
        '/assets/img/rooms/two-halfluxe/9.jpg',
        '/assets/img/rooms/two-halfluxe/10.jpg',
        '/assets/img/rooms/two-halfluxe/11.jpg',
        '/assets/img/rooms/two-halfluxe/12.jpg'
      ]
    },
    {
      name: 'Улучшенный трехместный номер',
      parameters: {
        rooms: 1,
        square: 30,
        residents: '3-4',
        price_start: 2400
      },
      equipment: {
        technology: [
          'холодильник',
          'TV',
          'Wi-Fi',
          'кондиционер /2 этаж/',
          'фен (по запросу)'
        ],
        hygiene: [
          'собственная ванная комната с душевой кабиной/ванной',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки'
        ],
        other: [
          'три односпальные кровати',
          'шкаф',
          'журнальный столик',
          'кресло',
          'диван',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/better-three/1.jpg',
        '/assets/img/rooms/better-three/2.jpg',
        '/assets/img/rooms/better-three/3.jpg',
        '/assets/img/rooms/better-three/4.jpg'
      ]
    },
    {
      name: 'Четырехместный номер',
      parameters: {
        rooms: 2,
        square: '30-45',
        residents: '4-5',
        price_start: 2800
      },
      equipment: {
        technology: [
          'TV',
          'Wi-Fi',
          'кондиционер /423/'
        ],
        hygiene: [
          'собственная ванная комната с душевой кабиной/ванной',
          'комплект косметических принадлежностей',
          'комплект полотенец'
        ],
        other: [
          'четыре односпальные кровати',
          'журнальный столик',
          'диван',
          'стол'
        ]
      },
      images: [
        '/assets/img/rooms/four/1.jpg',
        '/assets/img/rooms/four/2.jpg',
        '/assets/img/rooms/four/3.jpg',
        '/assets/img/rooms/four/4.jpg'
      ]
    },
    {
      name: 'Люкс',
      parameters: {
        rooms: 2,
        square: '45',
        residents: '2-4',
        price_start: 3200
      },
      equipment: {
        technology: [
          'холодильник',
          'TV',
          'Wi-Fi',
          'кондиционер',
          'фен (по запросу)'
        ],
        hygiene: [
          'собственная ванная комната с душевой кабиной/ванной',
          'комплект косметических принадлежностей',
          'комплект полотенец',
          'тапочки',
          'халат'
        ],
        other: [
          'двуспальная кровать',
          'шкаф',
          'журнальный столик',
          'кресло',
          'диван',
          'стол',
          'набор для чая',
          'набор посуды'
        ]
      },
      images: [
        '/assets/img/rooms/luxe/1.jpg',
        '/assets/img/rooms/luxe/2.jpg',
        '/assets/img/rooms/luxe/3.jpg',
        '/assets/img/rooms/luxe/4.jpg',
        '/assets/img/rooms/luxe/5.jpg',
        '/assets/img/rooms/luxe/6.jpg',
        '/assets/img/rooms/luxe/7.jpg',
        '/assets/img/rooms/luxe/8.jpg',
        '/assets/img/rooms/luxe/9.jpg',
        '/assets/img/rooms/luxe/10.jpg',
        '/assets/img/rooms/luxe/11.jpg',
        '/assets/img/rooms/luxe/12.jpg',
        '/assets/img/rooms/luxe/13.jpg',
        '/assets/img/rooms/luxe/14.jpg',
        '/assets/img/rooms/luxe/15.jpg',
        '/assets/img/rooms/luxe/16.jpg',
        '/assets/img/rooms/luxe/17.jpg',
        '/assets/img/rooms/luxe/18.jpg',
        '/assets/img/rooms/luxe/19.jpg',
        '/assets/img/rooms/luxe/20.jpg',
        '/assets/img/rooms/luxe/21.jpg',
        '/assets/img/rooms/luxe/22.jpg',
        '/assets/img/rooms/luxe/23.jpg',
        '/assets/img/rooms/luxe/24.jpg'
      ]
    }
  ]
}