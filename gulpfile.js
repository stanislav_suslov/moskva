const
    GULP_ISPROD = process.env.GULP_ISPROD == 1,
    browsersync = require('browser-sync').create(),
    // Gulp
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    tap = require('gulp-tap'),
    buffer = require('gulp-buffer'),
    plumber = require('gulp-plumber'),
    // HTML
    pug = require('gulp-pug'),
    // CSS
    cssimport = require('gulp-cssimport'),
    scss = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    cssnano = require('cssnano'),
    autoprefixer = require('autoprefixer'),
    // JS
    browserify = require('browserify'),
    babelify = require('babelify'),
    uglify = require('gulp-uglify');

const path = {
    html: {
        src: 'src/html/page/**/*.*',
        watch: 'src/html/**/*.*',
        base: 'src/html',
        dest: 'dist'
    },
    css: {
        src: 'src/css/application.scss',
        watch: 'src/css/**/*.*',
        base: 'src/css',
        dest: 'dist/assets/css'
    },
    js: {
        src: 'src/js/application.js',
        watch: 'src/js/**/*.*',
        base: 'src/js',
        dest: 'dist/assets/js'
    },
    public: {
        src: 'src/public/**/*.*',
        watch: 'src/public/**/*.*',
        base: 'src/public',
        dest: 'dist'
    },
};



/* ---------- Building tasks ---------- */

function html() {
    return gulp.src(path.html.src)
        .pipe(plumber())
        .pipe(pug({
            basedir: `${__dirname}/${path.html.base}`,
            data: {
                data: require('./data').get
            }
        }))
        .pipe(gulp.dest(path.html.dest))
        .pipe(browsersync.stream());
};

function css() {
    return gulp.src(path.css.src)
        .pipe(plumber())
        .pipe(scss())
        .pipe(cssimport({
            extensions: ['css']
        }))
        .pipe(gulpif(GULP_ISPROD, postcss([
            autoprefixer({
                browsers: ['defaults']
            }),
            cssnano({
                preset: ['default', {
                    discardComments: {
                        removeAll: true
                    }
                }]
            })
        ])))
        .pipe(gulp.dest(path.css.dest))
        .pipe(browsersync.stream());
};

function js() {
    return gulp.src(path.js.src, {
            read: false
        })
        .pipe(plumber())
        .pipe(tap(file => {
            file.contents = browserify(file.path, {
                    debug: true
                })
                .transform(babelify, {
                    presets: ['@babel/preset-env']
                })
                .bundle();
        }))
        .pipe(buffer())
        .pipe(gulpif(GULP_ISPROD, uglify()))
        .pipe(gulp.dest(path.js.dest))
        .pipe(browsersync.stream());
};

function public() {
    return gulp.src(path.public.src)
        .pipe(plumber())
        .pipe(gulp.dest(path.public.dest))
        .pipe(browsersync.stream());
};



/* ---------- Gulp tasks ---------- */

exports.build = gulp.parallel(html, css, js, public);

exports.dev = (cb) => {
    exports.build();

    browsersync.init({
        server: {
            baseDir: './dist'
        }
    });

    gulp.watch(path.html.watch, html);
    gulp.watch(path.css.watch, css);
    gulp.watch(path.js.watch, js);
    gulp.watch(path.public.watch, public);

    cb();
};