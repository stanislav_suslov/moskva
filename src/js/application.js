window.$ = window.jQuery = require('jquery'); // 'node_modules/jquery/dist/jquery.min.js',
import { WOW } from 'wowjs'; // 'node_modules/wowjs/dist/wow.min.js',
require('@fancyapps/fancybox'); // 'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
require('owl.carousel'); // 'node_modules/owl.carousel/dist/owl.carousel.min.js',
require('cleave.js'); // 'node_modules/cleave.js/dist/cleave.min.js',
require('cleave.js/dist/addons/cleave-phone.ru'); // 'node_modules/cleave.js/dist/addons/cleave-phone.ru.js',
require('daterangepicker'); // 'node_modules/daterangepicker/daterangepicker.js',

// Application
require('./form'); // 'src/js/form.js',
require('./modal'); // 'src/js/modal.js'

// wow.js init
;(function() {

	new WOW({
		mobile: false
	}).init();

})();

// img-slider
;(function() {

	$('.js-header-slider').owlCarousel({
		items: 1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 6000,
		smartSpeed: 1500,
		dots: false,
		mouseDrag: false,
		touchDrag: false,
		pullDrag: false

	});

	$('.js-img-slider').owlCarousel({
		items: 1,
		loop: true,
		autoplay: true,
		smartSpeed: 800,
		autoplayHoverPause: true
	});

})();