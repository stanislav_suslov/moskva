;(function($) {

	// Классы для формы .js-form-ajax
	const form = {
		container: 'js-form-ajax',
		group: 'js-form__group',
		groupError: 'form__group--error',
		input: 'js-form__input',
		select: 'js-form__select',
		date: 'js-form__date',
	};

	const daterangepicker_locale = {
		format: 'DD-MM-YYYY',
		applyLabel: 'Принять',
		cancelLabel: 'Отмена',
		invalidDateLabel: 'Выберите дату',
		daysOfWeek: ['Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс', 'Пн'],
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		firstDay: 1
	};

	/* -------------------- Инициализация -------------------- */

	$(`.${form.container}`).find(`.${form.date}`).each(function() {

		$(this).daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			minDate: new Date(),
			locale: daterangepicker_locale
		  });

	});



	/* -------------------- События -------------------- */

	// Отправка формы
	$(`.${form.container}`).submit(function(e) {
		e.preventDefault();
		var $this = $(this);
    var $inputs = $this.find(`.${form.group}`).find(`.${form.input}`);
    
    var formStatus = true;
    $inputs.map(function() {
      var status = validateInput(this);
      if (!status) {
        validInput(this, false);
        formStatus = false;
      }
    })

    if (formStatus) {
      $.ajax('/api.php', {
        data: $(this).serialize()
      }).then(response => {
        if (response === 'success') {
          $.fancybox.close();
          let timeout;
          $.fancybox.open({
            src: '#js-modal-success',
            opts: {
              touch: false,
              afterShow: () => {
                timeout = setTimeout(() => {
                  $.fancybox.close();
                }, 3000)
              },
              afterClose: () => {
                clearTimeout(timeout);
              }
            },
          });
        } else {
          throw new Error();
        }
      }).catch(reason => {
        alert('Неизвестная ошибка, пожалуйста, попробуйте позже');
      });
    }
	});

	// Сброс формы
	$(`.${form.container}`).on('reset', function() {

		var $this = $(this);
		var $inputs = $this.find(`.${form.group}`).find(`.${form.input}`);

		$inputs.each(function() {

			validInput(this, true);

		});

	});

	$(`.${form.input}`).on('input', function() { // Нет претензий в момент ввода

		validInput(this, true);

	});

	$(`.${form.input}`).on('blur', function() { // Вадилация поля

		var isValid = validateInput(this);
		validInput(this, isValid);

	});

	/**
	 * Визуализация валидности инпута
	 * @param {*} elem DOM-элемент
	 * @param {boolean} status Валидность ввода
	 */
	function validInput(elem, status) {

		if (status)
			$(elem).closest(`.${form.group}`).removeClass(form.groupError);
		else
			$(elem).closest(`.${form.group}`).addClass(form.groupError);

	}



	/* -------------------- Валидация [data-form-validator] -------------------- */

	function validateInput(input) {
		var $this = $(input);

    var validator = $this.data('form-validator');

    if (validator) {
      var validations = validator.split(' ');
      var value = $this.val();
      var errorMessages = [];
      var validateStatus = validations.every(function(validatorName) {
        var validator = validators[validatorName];
        var status = validator.check(value);
        if (status === false) {
          errorMessages.push(validator.messageError);
        }
  
        return status;
      });

      // console.log(`input: ${value} // errors: ${errorMessages.join(', ')}`);
      return validateStatus;
    }

    return true;
	}

	var validators = {

		required: {
			check: function(value) {
				return value.length ? true : false;
			},
			messageError: 'Поле должно быть заполнено'
		},

		string: {
			check: function(value) {
				return !/[^a-zA-Zа-яА-Я\s-]/i.test(value);
			},
			messageError: 'Введите строку'
		},

		name: {
			check: function(value) {
				return !/[^a-zA-Zа-яА-Я\s]/i.test(value);
			},
			messageError: 'Введите строку'
		},

		phone: {
			check: function(value) {
				var digitsCount = value.replace(/[^\d]/gm, '').length;
				return digitsCount >= 11 ? true : false;
			},
			messageError: 'Введите номер телефона в правильном формате'
		}

	};



	/* -------------------- Маска [data-form-mask] -------------------- */

	$(`.${form.input}`).each(function() {

		switch ($(this).data('form-mask')) {

			case 'phone':

				// mask
				var cleaverPhone = new Cleave(this, {
					phone: true,
					phoneRegionCode: 'RU'
				});

				// auto add '+7' value
				$(this).on('focus', function() {
					if ($(this).val().length === 0)
						cleaverPhone.setRawValue('+7');
				});

				break;

			case 'number':

				$(this).on('keypress keyup blur', function(e) {
					var $this = $(this);
					var val = $this.val();
					$this.val(val.replace(/[^0-9\.]/g,''));
					if ((e.which !== 46 || val.indexOf('.') !== -1) && (e.which < 48 || e.which > 57)) {
						e.preventDefault();
					}
				});

				break;

		}

	});


})(jQuery);