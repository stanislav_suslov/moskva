;(function($) {

	/**
	 * Entry point for modals
	 *
	 * First, let your modal name ex. 'popup'.
	 * Create your modal and add to container id attribute: #js-modal-popup
	 * After you need to open modal with link: .js-showmodal[data-modal-name=popup]
	 */
	$('html').on('click', '.js-showmodal', function(event) {

		event.preventDefault();

		var $this = $(this);
		var formName = $this.data('modal');
		var modalSelector = `#js-modal-${formName}`;

		/**
		 * Опциональные параметры, передаваемые через атрибуты.
		 *
		 * Используется массив элементов с полями name, type и value
		 * Для кнопки-триггера необходим атрибут "data-showmodal-attrs" в формате массива JSON-объектов
		 * В модальном окне для импорта используется атрибут "data-showmodal-import" со значением поля name
		 */
		var optionalAttrs = $this.data('showmodal-attrs');

		if (Array.isArray(optionalAttrs)) {
			try {
				var modal = $(modalSelector);

				optionalAttrs.forEach(function(element) {
					var needle = modal.find('[data-showmodal-import='+element.name+']');
          var val = element.value;
          
          console.log(needle);

					switch (element.type) {
						case 'val':  needle.val(val);  break;
						case 'html': needle.html(val); break;
						case 'text': needle.text(val); break;
					}
				});
			} catch (e) {
				console.log('failed to parse showmodal attributes. error message:');
				console.log(e);
			}
		}

		// Open modal
		$.fancybox.open({
			src: modalSelector,
			opts: {
				touch: false,
				afterClose: function() {
					$(modalSelector).find('form').each(function() {
						this.reset();
					})
				},
			}
		});

	});

	// Settings for galleries
	$('[data-fancybox^="gallery"]').fancybox({
		buttons: [
			"thumbs",
			"close"
		]
	});

})(jQuery);