<?php
header("Access-Control-Allow-Origin: *");

$input = filter_var_array($_GET, FILTER_SANITIZE_STRING);

switch ($input['form_type']) {
  case 'callback':
    $status = mail('hm650706@yandex.ru', 'Форма "Обратный звонок" (Гостиница Москва)', implode(PHP_EOL, [
      "Заполнена заявка с сайта",
      "",
      "Имя: {$input['name']}",
      "Телефон: {$input['phone']}",
    ]), 'From: mail@hotelmsk45.ru');
    echo $status ? 'success' : 'error';
    break;

  case 'booking':
    $status = mail('hm650706@yandex.ru', 'Форма "Бронь номера" (Гостиница Москва)', implode(PHP_EOL, [
      "Заполнена заявка с сайта",
      "",
      "ФИО: {$input['fio']}",
      "Телефон: {$input['phone']}",
      "Эл. почта: {$input['email']}",
      "Номер: {$input['room']}",
      "Питание: {$input['food']}",
      "Дата заезда: {$input['date_enter']}",
      "Время заезда: {$input['time_enter']}",
      "Дата выезда: {$input['date_exit']}",
      "Количество человек: {$input['count_people']}",
      "Количество номеров: {$input['count_rooms']}",
      "Комментарий: {$input['comment']}",
    ]));
    echo $status ? 'success' : 'error';
    break;

  default:
    echo 'API method not found';
    break;
}
?>